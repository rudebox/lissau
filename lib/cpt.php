<?php 
// Register Custom Post Type solafskærmning
function create_solafskrmning_cpt() {

	$labels = array(
		'name' => _x( 'Solafskærmning', 'Post Type General Name', 'lissau' ),
		'singular_name' => _x( 'Solafskærmning', 'Post Type Singular Name', 'lissau' ),
		'menu_name' => _x( 'Solafskærmning', 'Admin Menu text', 'lissau' ),
		'name_admin_bar' => _x( 'Solafskærmning', 'Add New on Toolbar', 'lissau' ),
		'archives' => __( 'Solafskærmning Archives', 'lissau' ),
		'attributes' => __( 'Solafskærmning Attributes', 'lissau' ),
		'parent_item_colon' => __( 'Parent solafskærmning:', 'lissau' ),
		'all_items' => __( 'All Solafskærmning', 'lissau' ),
		'add_new_item' => __( 'Add New Solafskærmning', 'lissau' ),
		'add_new' => __( 'Add New', 'lissau' ),
		'new_item' => __( 'New Solafskærmning', 'lissau' ),
		'edit_item' => __( 'Edit Solafskærmning', 'lissau' ),
		'update_item' => __( 'Update Solafskærmning', 'lissau' ),
		'view_item' => __( 'View Solafskærmning', 'lissau' ),
		'view_items' => __( 'View Solafskærmning', 'lissau' ),
		'search_items' => __( 'Search Solafskærmning', 'lissau' ),
		'not_found' => __( 'Not found', 'lissau' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'lissau' ),
		'featured_image' => __( 'Featured Image', 'lissau' ),
		'set_featured_image' => __( 'Set featured image', 'lissau' ),
		'remove_featured_image' => __( 'Remove featured image', 'lissau' ),
		'use_featured_image' => __( 'Use as featured image', 'lissau' ),
		'insert_into_item' => __( 'Insert into Solafskærmning', 'lissau' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Solafskærmning', 'lissau' ),
		'items_list' => __( 'Solafskærmning list', 'lissau' ),
		'items_list_navigation' => __( 'Solafskærmning list navigation', 'lissau' ),
		'filter_items_list' => __( 'Filter Solafskærmning list', 'lissau' ),
	);
	$args = array(
		'label' => __( 'Solafskærmning', 'lissau' ),
		'description' => __( '', 'lissau' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-archive',
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'page-attributes', 'post-formats'),
		'taxonomies' => array('category'),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'Solafskaermning', $args );

}
add_action( 'init', 'create_solafskrmning_cpt', 0 );

// Register Custom Post Type av
function create_av_cpt() {

	$labels = array(
		'name' => _x( 'AV', 'Post Type General Name', 'lissau' ),
		'singular_name' => _x( 'av', 'Post Type Singular Name', 'lissau' ),
		'menu_name' => _x( 'av', 'Admin Menu text', 'lissau' ),
		'name_admin_bar' => _x( 'av', 'Add New on Toolbar', 'lissau' ),
		'archives' => __( 'av Archives', 'lissau' ),
		'attributes' => __( 'av Attributes', 'lissau' ),
		'parent_item_colon' => __( 'Parent av:', 'lissau' ),
		'all_items' => __( 'All av', 'lissau' ),
		'add_new_item' => __( 'Add New av', 'lissau' ),
		'add_new' => __( 'Add New', 'lissau' ),
		'new_item' => __( 'New av', 'lissau' ),
		'edit_item' => __( 'Edit av', 'lissau' ),
		'update_item' => __( 'Update av', 'lissau' ),
		'view_item' => __( 'View av', 'lissau' ),
		'view_items' => __( 'View av', 'lissau' ),
		'search_items' => __( 'Search av', 'lissau' ),
		'not_found' => __( 'Not found', 'lissau' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'lissau' ),
		'featured_image' => __( 'Featured Image', 'lissau' ),
		'set_featured_image' => __( 'Set featured image', 'lissau' ),
		'remove_featured_image' => __( 'Remove featured image', 'lissau' ),
		'use_featured_image' => __( 'Use as featured image', 'lissau' ),
		'insert_into_item' => __( 'Insert into av', 'lissau' ),
		'uploaded_to_this_item' => __( 'Uploaded to this av', 'lissau' ),
		'items_list' => __( 'av list', 'lissau' ),
		'items_list_navigation' => __( 'av list navigation', 'lissau' ),
		'filter_items_list' => __( 'Filter av list', 'lissau' ),
	);
	$args = array(
		'label' => __( 'av', 'lissau' ),
		'description' => __( '', 'lissau' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-archive',
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'page-attributes', 'post-formats'),
		'taxonomies' => array('category'),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'av', $args );

}
add_action( 'init', 'create_av_cpt', 0 );
 ?>