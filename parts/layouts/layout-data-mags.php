<?php 
/**
* Description: Lionlab product data repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//section settings 
$section_title = get_sub_field('header');
$margin = get_sub_field('margin');
$bg = get_sub_field('bg');

$header = get_sub_field('data-mag_title');

if (have_rows('data-mags') ) :
?>

<section id="product-data" class="product padding--<?php echo esc_attr($margin); ?> bg--<?php echo esc_attr($bg); ?>">
	<div class="wrap hpad">
		<div class="row flex flex--wrap">

			<div class="col-sm-8 col-sm-offset-2 product__data-list">
				<?php if ($section_title) : ?>
				<h2 class="product__data-header"><?php echo esc_html($section_title); ?></h2>
				<?php endif; ?>				
				<?php if ($header) : ?>
				<h3 class="product__data-header"><?php echo esc_html($header); ?></h3>
				<?php endif; ?>
				<?php while (have_rows('data-mags') ) : the_row(); 
					$file = get_sub_field('pdf_file');
					$title = get_sub_field('data_title');
				?>

				<a target="_blank" href="<?php echo esc_url($file['url']); ?>" download class="product__data-item" itemscope itemtype="DigitalDocument">
					
					<p class="product__data-title"><?php echo esc_html($title); ?></p>
					<i class="product__data-icon"><?php echo file_get_contents('wp-content/themes/lissau/assets/img/download.svg'); ?></i>
				</a>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>