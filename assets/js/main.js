function curtainCall() {
    var curtainMain = $('.curtain'),
        curtainSub = $('.curtain__item');

        curtainSub.addClass('remove');    
        curtainMain.addClass('remove');
};

function curtainCallReverse(callback) {
    var curtainMain = $('.curtain'),
        curtainSub = $('.curtain__item');

        curtainMain.removeClass('remove');
        curtainSub.removeClass('remove');

    setTimeout(function() {
        callback(true);
    }, 700);
};


jQuery(document).ready(function($) {

  
  setTimeout(function() {
      curtainCall();
  }, 100);

    $('.nav__item a, .header__logo, .archive__item, .single__cat-link a, .product-sort__btn, .single__btn').click(function (e) {
        var url = $(this).attr('href');

        e.preventDefault();

        if (url.indexOf("#") === 0) {
            location.href = url;
        } else {
            curtainCallReverse(function(ret) {
                if (ret) {
                    location.href = url;
                }
            });
        }
  });


  //mixit up
  if($('body').is('.home')) {

    // Instantiate and configure the mixer
    var mixer = mixitup('.mixit', {
      
    });

  }
  


  //in viewport check
  function viewportCheck() {
    var $animation_elements = $('.is-animated');
    var $window = $(window);

    function check_if_in_view() {
      var window_height = $window.height();
      var window_top_position = $window.scrollTop();
      var window_bottom_position = (window_top_position + window_height);
     
      $.each($animation_elements, function() {
        var $element = $(this);
        var element_height = $element.outerHeight();
        var element_top_position = $element.offset().top;
        var element_bottom_position = (element_top_position + element_height);
     
        //check to see if this current container is within viewport
        if ((element_top_position <= window_bottom_position)) {
          $element.addClass('in-view');
        } else {
          $element.removeClass('in-view');
        }
      });
    }

    $window.on('scroll resize', check_if_in_view);
    $window.trigger('scroll');
  }

  viewportCheck();

  //menu toggle
  $('.nav-toggle').click(function(e) {
    e.preventDefault();
    $('.nav--mobile').toggleClass('is-open');
    $('body').toggleClass('is-fixed');
    $('.nav__item').toggleClass('is-animated');
  });


  //Smooth scroll
  $('a[href^="#"]').on('click',function (e) {
    e.preventDefault();

    var target = this.hash;
    var $target = $(target);

    $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  }); 



  //owl slider/carousel
  var owl = $('.slider__track');

  owl.each(function() {
    $(this).children().length > 1;

    owl.owlCarousel({
        loop: true,
        items: 1,
        autoplay: true,
        mouseDrag: false,
        // nav: true,
        dots: false, 
        autplaySpeed: 11000,
        autoplayTimeout: 10000,
        smartSpeed: 2200,
        navSpeed: 2200
        // navText : ["<i class='fa fa-arrow-left' aria-hidden='true'></i>", "<i class='fa fa-arrow-right' aria-hidden='true'></i>"]
    });
  });

  //owl slider/carousel
  var owlSingle = $('.slider__track--single');

  owlSingle.each(function() {
    $(this).children().length > 1;

    owlSingle.owlCarousel({ 
        loop: true,
        items: 1,
        autoplay: true,
        mouseDrag: false,
        nav: true,
        dots: true,
        dotsContainer: '#owl-dots', 
        dotsData: true,
        autplaySpeed: 11000,
        autoplayTimeout: 10000,
        smartSpeed: 2200,
        navSpeed: 2200, 
        navText : [""],
        responsive : {
          // breakpoint from 0 up
          0 : {
              nav: false
          },
          // breakpoint from 768 up
          768 : {
              nav: true
          }
        }
    });
  });


  //owl slider/carousel
  var owlSingleB2b = $('.slider__track--single--b2b');

  owlSingleB2b.each(function() {
    $(this).children().length > 1;

    owlSingleB2b.owlCarousel({ 
        loop: true,
        items: 1,
        autoplay: true,
        mouseDrag: false,
        nav: true,
        dots: true,
        dotsContainer: '#owl-dots-b2b', 
        dotsData: true,
        autplaySpeed: 11000,
        autoplayTimeout: 10000,
        smartSpeed: 2200,
        navSpeed: 2200, 
        navText : [""],
        responsive : {
          // breakpoint from 0 up
          0 : {
              nav: false
          },
          // breakpoint from 768 up
          768 : {
              nav: true
          }
        }
    });
  });


  $('.owl-dot--thumb').click(function() {
      owlSingle.trigger('to.owl.carousel', [$(this).index(), 1000]);
  });

  $('.owl-dot--thumb--b2b').click(function() {
      owlSingleB2b.trigger('to.owl.carousel', [$(this).index(), 1000]);
  });


  //owl slider/carousel
  var owl = $('.slider__track--variations');

  owl.each(function() {
    $(this).children().length > 1;

    owl.owlCarousel({ 
        loop: true,
        items: 6,
        autoplay: true,
        nav: true,
        dots: false,
        autplaySpeed: 11000,
        autoplayTimeout: 10000,
        smartSpeed: 2200,
        navSpeed: 2200, 
        navText : [""],
        responsiveClass: true,
        responsive : {
          // breakpoint from 0 up
          0 : {
              items: 2,
              nav: true
          },
          // breakpoint from 0 up
          480 : {
              items: 3,
              nav: true
          },
          // breakpoint from 768 up
          768 : {
              items: 4,
              nav: true
          },
          // breakpoint from 992 up
          992 : {
              items: 6,
              nav: true
          }
        }
    });
  });

  //owl slider/carousel
  var owl = $('.slider__track--products');

  owl.each(function() {
    $(this).children().length > 1;

    owl.owlCarousel({ 
        loop: true,
        items: 3,
        autoplay: true,
        dots: false,
        margin: 30,
        nav: true,
        autplaySpeed: 110,
        autoplayTimeout: 10000,
        smartSpeed: 2200,
        navSpeed: 2200, 
        navText : [""],
        responsiveClass: true,
        responsive : {
          // breakpoint from 0 up
          0 : {
              items: 1,
              nav: true
          },
          // breakpoint from 768 up
          768 : {
              items: 2,
              nav: true
          },
          // breakpoint from 992 up
          992 : {
              items: 3,
              nav: true
          }
        }
    });
  });


  //redirect to all categories if all categories selected 
  $('.archive__filter-select--sol').change(function()  {
    if ($(this).val() === '0') {
      window.location.replace('/solafskaermning');
    }
  });


  //redirect to all categories if all categories selected 
  $('.archive__filter-select--av').change(function()  {
    if ($(this).val() === '0') {
      window.location.replace('/av');
    }
  });


  //fancybox
  $('[data-fancybox]').fancybox({   
    toolbar  : false,
    smallBtn : true,
    iframe : { 
      preload : false 
    } 
  });


  //check for hash in URL - maybe rewrite this in near future to output no HTML at all if conditions met, instead of hiding it.
  if (window.location.href.indexOf('#b2b') > -1) {
       $('.slider--private').addClass('is-hidden');
       $('.slider--b2b').removeClass('is-hidden');
  }


  $('#js-btn-sol').on('click', function() {
    $('.customer-service__intro').addClass('is-hidden');
    $('#js-col-sol').toggleClass('is-hidden');
    $('.customer-service__list').toggleClass('is-active');
  });

  $('#js-btn-av').on('click', function() {
    $('.customer-service__intro').addClass('is-hidden');
    $('#js-col-av').toggleClass('is-hidden');
    $('.customer-service__list').toggleClass('is-active');
  });

});
