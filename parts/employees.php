<?php 
	$title = get_field('employees_header', 'options');
?>


<section class="employees padding--both" itemscope itemtype="http://schema.org/employees">

	<div class="wrap hpad">
		<div class="row">

			<div class="col-sm-8 col-sm-offset-2">
				
				<?php if ($title) : ?>
				<h2 class="employees__header"><?php echo esc_html($title); ?></h2>
				<?php endif; ?>
			
				<div class="row flex flex--wrap">
					<?php if (have_rows('employees', 'options') ) : while (have_rows('employees', 'options') ) : 	the_row(); 

					 $img = get_sub_field('img');
 					 $position = get_sub_field('position');
 					 $name = get_sub_field('name');
 					 $phone = get_sub_field('phone');
 					 $mail = get_sub_field('mail'); 
 					?>
					
 					<div class="employees__employee center col-sm-6">
 						
 						<h3 class="employees__name"><?php echo esc_html($name); ?></h3>
						
						<?php if ($img) : ?>
 						<img loading="lazy" class="employees__img" src="<?php echo esc_url($img['sizes']['thumbnail']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
 						<?php endif; ?>

 						<?php if ($position) : ?>
 						<p><?php echo esc_html($position); ?></p>
 						<?php endif; ?>
						
						<div class="employees__wrap">
 						<?php if ($phone) : ?>
 						Telefon: <a href="tel:<?php echo get_formatted_phone($phone); ?>"><?php echo esc_html($phone); ?></a><br>
 						<?php endif; ?>

 						<?php if ($mail) : ?>
 						E-mail: <a href="mailto:<?php echo esc_html($mail); ?>"><?php echo esc_html($mail); ?></a>
 						<?php endif; ?>
 						</div>
						  						
 					</div>

 					<?php endwhile; endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
	
