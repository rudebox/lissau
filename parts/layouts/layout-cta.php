<?php
/**
 * Description: Lionlab layout cta field group
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

//section settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$bg_img = get_sub_field('bg_img');

$title = get_sub_field('cta_title');
$text = get_sub_field('cta_text');
$position = get_sub_field('cta_position');

if ($position === 'left') {
	$position = 'col-sm-6';
}

if ($position === 'right') {
	$position = 'col-sm-6 col-sm-push-6';
	$section_class = 'right';
}
?>

<section loading="lazy" class="cta--layout <?php echo esc_attr($bg); ?>-gradient margin--<?php echo esc_attr($margin); ?> <?php echo esc_attr($section_class); ?>" style="background-image: url(<?php echo $bg_img['url']; ?>);">
	<div class="wrap hpad cta__container">
		<?php if ($position) : ?>
		<div class="row">
			<div class="cta__col <?php echo esc_attr($position); ?>">
			<?php endif; ?>
				<h2 class="cta__title"><?php echo esc_html($title); ?></h2>
				<?php echo $text; ?>
			</div>
		</div>
	</div>
</section>