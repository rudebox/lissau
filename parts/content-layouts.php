<?php
if( have_rows('layout') ) {
  $GLOBALS['layout_count'] = 1; while ( have_rows('layout') ) { the_row();
    if( get_row_layout() === 'slider' ) { ?>

      <?php get_template_part( 'parts/layouts/layout', 'slider' ); ?>

    <?php
    } elseif( get_row_layout() === 'link-boxes' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'link-boxes' ); ?>

    <?php
    } elseif( get_row_layout() === 'product-sort' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'product-sort' ); ?>

    <?php
    } elseif( get_row_layout() === 'wysiwygs' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'wysiwygs' ); ?>

    <?php
    } elseif( get_row_layout() === 'materials' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'materials' ); ?>

    <?php
    } elseif( get_row_layout() === 'data-mag' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'data-mags' ); ?> 

    <?php
    } elseif( get_row_layout() === 'customer-service-cats' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'customer-service-cats' ); ?> 

    <?php
    } elseif( get_row_layout() === 'process' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'process' ); ?>

    <?php
    } elseif( get_row_layout() === 'product-slider' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'product-slider' ); ?>

    <?php
    } elseif( get_row_layout() === 'cta' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'cta' ); ?>

    <?php
    } elseif( get_row_layout() === 'intro' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'intro' ); ?>

    <?php
    } elseif( get_row_layout() === 'portrait' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'portraits' ); ?>

    <?php
    }
    ?>

  <?php
    $GLOBALS['layout_count']++;
  }
  ?>

<?php
} else {
?>
  <p class="center">You haven't added any layouts yet. <?php edit_post_link('Add one now.'); ?></p>
<?php
}
?>
