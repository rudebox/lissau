<?php 
/**
* Description: Lionlab customer service cats repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

?>

<section class="customer-service bg--<?php echo esc_attr($bg); ?> padding--<?php echo esc_attr($margin); ?>">

	<?php if ($title) : ?>
		<div class="wrap hpad">
			<h2 class="customer-service__header center"><?php echo esc_html($title); ?></h2>
		</div>
	<?php endif; ?>

	<section class="customer-service__intro">
		<div class="wrap hpad">
			<div class="row">
				<button id="js-btn-sol" class="customer-service__cats col-sm-6 h2"><?php echo __('Solafskærmning'); ?></button>
				<button id="js-btn-av" class="customer-service__cats col-sm-6 h2"><?php echo __('AV'); ?></button>
			</div>
		</div>
	</section>

	<?php 
	if (have_rows('sol_cats') ) :
	?>
	<div id="js-col-sol" class="wrap hpad is-hidden customer-service__list">
		<div class="row flex flex--wrap">
			<?php while (have_rows('sol_cats') ) : the_row(); 
				$title = get_sub_field('title');
				$img = get_sub_field('icon');
				$link = get_sub_field('link');
			?>

			<a href="<?php echo esc_url($link); ?>" class="col-sm-4 customer-service__cats-item is-animated fade-up">
				<h4><?php echo esc_html($title); ?></h4>
			</a>
			<?php endwhile; ?>
		</div>
	</div>
	<?php endif; ?>

	<?php 
		if (have_rows('av_cats') ) :
	?>
	<div id="js-col-av" class="wrap hpad is-hidden customer-service__list">
		<div class="row flex flex--wrap">
			<?php while (have_rows('av_cats') ) : the_row(); 
				$title = get_sub_field('title');
				$img = get_sub_field('icon');
				$link = get_sub_field('link');
			?>

			<a href="<?php echo esc_url($link); ?>" class="col-sm-4 customer-service__cats-item is-animated fade-up">
				<h4><?php echo esc_html($title); ?></h4>
			</a>
			<?php endwhile; ?>
		</div>
	</div>
	<?php endif; ?>
</section>



