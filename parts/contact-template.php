<?php 
/*
* Template Name: Contact
*/

//contact fields

get_template_part('parts/header'); the_post(); ?> 

<main>
	<?php get_template_part('parts/google', 'maps'); ?>
	
	<?php get_template_part('parts/page', 'header');?>

	<?php get_template_part('parts/employees'); ?>

	<?php get_template_part('parts/contact-customer', 'service'); ?>

	<section class="contact padding--both" itemscope itemtype="http://schema.org/LocalBusiness">
		<div class="wrap hpad">
			<div class="row">
				
				<div class="col-sm-8 col-sm-offset-2 contact__item">					
					<?php gravity_form( 2, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
				</div>

			</div>
		</div>
	</section>
</main>

<?php get_template_part('parts/footer'); ?>