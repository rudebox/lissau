<?php 
	$title = get_field('product_inquiry_title', 'options');
	$text = get_field('product_inquiry_text', 'options');
 ?>

<section id="product-inquiry" class="product padding--both bg--beige">
 	<div class="wrap hpad">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2 product__inquiry-item">
				 <h2 class="product__inquiry-title"><?php echo esc_html($title); ?></h2>
 				<?php echo $text; ?>
				<?php  
					gravity_form( 1, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, $tabindex, $echo = true );
				?>
			</div>
		</div>
	</div>
</section>
