<?php get_template_part('parts/header'); ?>

<main>

<?php get_template_part('parts/page', 'header'); ?>

  <section class="blog padding--both">
    <div class="wrap hpad">
      <div class="row">

        <?php if (have_posts()): ?>
          <?php while (have_posts()): the_post(); ?>

          <?php   
            //get thumb
            $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' ); 
          ?>

          <a class="blog__post col-sm-4" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" itemscope itemtype="http://schema.org/BlogPosting" style="background-image: url(<?php echo $thumb[0]; ?>);">

            <header class="blog__header">
              <h3 itemprop="headline">              
                  <?php the_title(); ?>
              </h3>
            </header>

            <div class="blog__excerpt" itemprop="articleBody">
              <?php the_excerpt(); ?>
            </div>

          </a>

          <?php endwhile; else: ?>

            <p>No posts here.</p>

        <?php endif; ?>

      </div>
    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>