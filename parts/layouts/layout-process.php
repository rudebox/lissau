<?php 
/**
* Description: Lionlab process repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

if (have_rows('process_box') ) :

//counter
$i=0;
?>

<section class="process bg--<?php echo esc_attr($bg); ?> padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<?php if ($title) : ?>
		<h2 class="process__header is-animated fade-in"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>
		<div class="row flex flex--wrap">
			<?php while (have_rows('process_box') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$icon = get_sub_field('icon');

				$i++;
			?>

			<div class="process__item is-animated fade-up bg--beige">
				<i class="process__icon"><?php echo esc_html($i); ?></i>
				<h3 class="process__title"><?php echo esc_html($title); ?></h3>
				<?php echo $text; ?>
			</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>