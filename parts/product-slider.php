<?php
/**
 * Description: Lionlab product slider
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

$bg = get_field('product_slider_bg');

if ( have_rows('product_slider') ) : ?>

  <section class="slider slider--private">
    <div class="slider__track--single is-slider">
    
      <?php

      // Loop through slides
      while ( have_rows('product_slider') ) :
        the_row();
        $img   = get_sub_field('img'); 
        ?>

        <div class="slider__item flex flex--valign" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
        </div>

      <?php endwhile; ?>

    </div>

    <div class="is-slider is-slider--thumbs">
      <ul id="owl-dots" class="owl-dots flex flex--wrap flex--valign"> 

        <?php 
          while (have_rows('product_slider') ) : the_row(); 
            $img = get_sub_field('img');
            $i = get_row_index();
        ?>
          <li data-dot="<?php echo esc_attr($i); ?>" loading="lazy" class="owl-dot owl-dot--thumb" style="background-image: url(<?php echo esc_url($img['url']); ?>);"></li> 
        <?php endwhile; ?>

        </ul>
      </div>

    <?php get_template_part('parts/page', 'header'); ?>
  </section>
<?php endif; ?>


<?php if ( have_rows('product_slider_b2b') ) : ?>

  <section class="slider slider--b2b is-hidden">
    <div class="slider__track--single--b2b is-slider">
    
      <?php

      // Loop through slides
      while ( have_rows('product_slider_b2b') ) :
        the_row();
        $image   = get_sub_field('img'); 
        ?>

        <div class="slider__item flex flex--valign" style="background-image: url(<?php echo esc_url($image['url']); ?>);">
        </div>

      <?php endwhile; ?>

    </div>

    <div class="is-slider is-slider--thumbs">
      <ul id="owl-dots-b2b" class="owl-dots flex flex--wrap flex--valign"> 

        <?php 
          while (have_rows('product_slider_b2b') ) : the_row(); 
            $image = get_sub_field('img');
            $index = get_row_index();
        ?>
          <li data-dot="<?php echo esc_attr($index); ?>" loading="lazy" class="owl-dot owl-dot--thumb--b2b" style="background-image: url(<?php echo esc_url($image['url']); ?>);"></li> 
        <?php endwhile; ?>

        </ul>
      </div>

    <?php get_template_part('parts/page', 'header'); ?>
  </section>
<?php endif; ?>