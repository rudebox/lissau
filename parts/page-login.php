<?php
/**
 * Template Name: Login
 **/

get_template_part('parts/header'); 

$text = get_field('login_form_text');
?>

<?php get_template_part('parts/page', 'header'); ?>

<main>
	<section class="login padding--both">
		<div class="wrap hpad">
			<div class="row">

				<div class="col-sm-8 col-sm-offset-2 login__form bg--beige">
					
					<h3><?php echo $text; ?></h3>

					<?php $login  = (isset($_GET['login']) ) ? $_GET['login'] : 0;

					if ( $login === "failed" ) {
						echo '<p class="login__message">' . __('Error', 'lissau') . ': ' . __('Invalid username and/or password.', 'lissau') . '</p>';
					} elseif ( $login === "empty" ) {
						echo '<p class="login__message">' . __('Error', 'lissau') . ': ' . __('Username and/or Password is empty.', 'lissau') . '</p>';
					} elseif ( $login === "false" ) {
						echo '<p class="login__message">' . __('Error', 'lissau') . ': ' . __('You are logged out.', 'lissau') . '</p>';
					}

					$args = array(
						'redirect' => '/forhandler'
					);

					wp_login_form($args); ?>

				</div>

			</div>
		</div>
	</section>
</main>

<?php get_template_part('parts/footer'); ?>