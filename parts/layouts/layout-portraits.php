<?php 
/**
* Description: Lionlab portraits repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');

//materials
$title = get_sub_field('title');
$text = get_sub_field('text');
$img = get_sub_field('img');
$img_color = get_sub_field('img_bg_color');
$img_name = get_sub_field('img_name');
$img_title = get_sub_field('img_title');
$position = get_sub_field('position');

if ($position === 'left') {
	$img_class = 'col-sm-offset-3';
	$col_class = 'col-sm-offset-1 col-lg-offset-3';
}

if ($position === 'right') {
	$img_class = 'col-sm-pull-3';
}
?>

<section class="portraits <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<div class="portraits__row row flex flex--wrap <?php echo esc_attr($position); ?>">

			<div class="col-sm-5 col-lg-4 portraits__item portraits__item--img <?php echo esc_attr($position); ?> bg--<?php echo esc_attr($img_color); ?>">
				<?php if ($img_name && $img_title) : ?>				
				<div class="portraits__description">
					<?php echo esc_html($img_name); ?><i></i><?php echo esc_html($img_title); ?>
				</div>
				<?php endif; ?>

				<?php if ($img) : ?>
				<img loading="lazy" class="portraits__img <?php echo esc_attr($img_class); ?>" src="<?php echo esc_url($img['sizes']['side-img']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
				<?php endif; ?>
			</div>

			<div class="portraits__item portraits__item--text col-sm-6 col-lg-5 <?php echo esc_attr($col_class); ?> <?php echo esc_attr($position); ?>">
				<h3 class="portraits__title"><?php echo $title; ?></h3>
				<?php echo $text; ?>
			</div>
			
		</div>
	</div>
</section>