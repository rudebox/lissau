<?php 
  //section settings 
  $section_title = get_sub_field('header');
  $section_text = get_sub_field('header_text');
  $margin = get_sub_field('margin');
  $bg = get_sub_field('bg');
?>

<section class="product-sort padding--<?php echo esc_attr($margin); ?>">
  <div class="wrap hpad">
    
    <?php if ($section_title) : ?>
      <h2 class="product-sort__header center"><?php echo esc_html($section_title); ?></h2>
    <?php endif; ?>
    

    <?php if ($section_text) : ?>
      <div class="row">
        <div class="product-sort__header-text col-sm-8 col-sm-offset-2 center"><?php echo esc_html($section_text); ?></div>
      </div>  
    <?php endif; ?>

    <?php
      //sol
      $category_name = 'solafskaermning';
      $category = get_category_by_slug( $category_name );
      $category_id = $category->term_id;

      $args = array(
        'child_of' => $category_id
      );

      $categories = get_categories( $args );
    ?>

    <div class="product-sort__controls flex flex--wrap">
      
        <div class="product-sort__filter blog__filter--all btn" data-filter="all">Alle</div>

        <?php foreach($categories as $category) : ?>          
          <div class="product-sort__filter btn" data-filter=".cat<?php echo $category->term_id;?>"><?php echo $category->name; ?></div>  
        <?php endforeach; ?>

        <div class="product-sort__filter btn" data-filter=".cat4">Lærreder, lærredstilbehør & hjemmebiograf</div> 
    

    </div>

    <div class="row flex flex--wrap">
        <div class="mixit product-sort__row flex flex--wrap">

        <?php 
            $args = array( 
              'post_type' => array('Solafskaermning', 'AV'),
              'posts_per_page' => -1,
              'orderby' => 'date',
              'order'   => 'ASC'

            );
            $query = new WP_Query( $args );
         ?>

        <?php if ($query->have_posts()): ?>
            <?php while ($query->have_posts()): $query->the_post(); ?>

          <?php 
              $cats = get_the_category();
              $cat_string = ""; 

              foreach ($cats as $cat) {
                $cat_string .= " cat" . $cat->term_id ."";
              }
          ?>

          <?php 
              //post img
              $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'cats' );
              //post img alt tag
              $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true);   
          ?>

          <div title="<?php the_title_attribute(); ?>" class="product-sort__item col-sm-4 mix <?php echo $cat_string; ?>" itemtype="http://schema.org/Product" itemscope>

            <img loading="lazy" itemprop="thumbnail" src="<?php echo esc_url($thumb[0]); ?>" alt="<?php echo $alt; ?>">

            <header>
              <h2 class="product-sort__title h4" itemprop="headline">                                
                  <?php the_title(); ?>
              </h2>
            </header>

            <div class="product-sort__excerpt" itemprop="articleBody">
              <?php the_excerpt(); ?>
            </div>
             
            <div class="product-sort__btn-wrap">
              <a href="<?php the_permalink(); ?>" class="btn btn--brown product-sort__btn">Privat</a> <a href="<?php the_permalink(); ?>#b2b" class="btn btn--brown product-sort__btn product-sort__btn--b2b">Erhverv</a> 
            </div> 

          </div>

          <?php wp_reset_postdata(); ?>

          <?php endwhile; else: ?>

            <p>No posts here.</p>

        <?php endif; ?>
      </div>

    </div>
  </div>

</section>