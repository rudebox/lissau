<?php
/**
 * Description: Lionlab layout product slider
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

$header = get_sub_field('slides_header');

if ( have_rows('slides') ) : ?>
  
 <section class="slider slider--products padding--top">

    <div class="wrap--fluid">
      <?php if ($header) : ?>
      <h2 class="slider__header center"><?php echo esc_html($header); ?></h2>
      <?php endif; ?>
      <div class="slider__track--products is-slider">

        <?php
        // Loop through slides
        while ( have_rows('slides') ) :
          the_row();
            $image   = get_sub_field('slides_bg');
            $title   = get_sub_field('slides_title'); 
            $link   = get_sub_field('slides_link'); 
          ?>

          <a href="<?php echo esc_url($link); ?>" class="slider__item flex flex--valign" style="background-image: url(<?php echo esc_url($image['url']); ?>);">
            
            <?php if ($title) : ?>
            <h3><?php echo esc_html($title); ?></h3>
            <?php endif; ?>
          </a>

        <?php endwhile; ?>

      </div>
    </div>
  </section>
<?php endif; ?>