<?php
/**
 * Description: Lionlab cta field group
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

//section settings
$bg = get_field('cta_bg', 'options');

$title = get_field('cta_title', 'options');
$link = get_field('cta_link', 'options');
$link_text = get_field('cta_link_text', 'options');
if( $link ): 
$link_url = $link['url'];
$link_target = $link['target'] ? $link['target'] : '_self';
endif;
?>

<section class="cta bg--<?php echo esc_attr($bg); ?>">
	<div class="wrap hpad center cta__container">
		<h2 class="cta__title"><?php echo esc_html($title); ?></h2>
		<a class="btn btn--brown cta__btn" target="<?php echo esc_attr($link_target); ?>" href="<?php echo esc_url($link_url); ?>"><?php echo esc_html($link_text); ?></a>
	</div>
</section>