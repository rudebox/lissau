<?php
/**
 * Description: Lionlab csutomer service cta field group
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

//section settings
$bg = get_field('customer_service_bg');

$title = get_field('customer_service_title');
$link = get_field('customer_service_link');
$link_text = get_field('customer_service_link_text');

if ($bg === 'beige') {
	$btn_class = 'brown';
}

if ($bg === 'brown') {
	$btn_class = 'beige';
}

if ($bg === 'none') { 
	$btn_class = 'brown';
}
?>

<section class="customer-service customer-service--cta bg--<?php echo esc_attr($bg); ?> margin--both wrap hpad">
	<div class="customer-service__container flex flex--wrap flex--justify">
		<h2 class="customer-service__title"><?php echo esc_html($title); ?></h2>
		<a class="btn btn--brown customer-service__btn" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a>
	</div>
</section>