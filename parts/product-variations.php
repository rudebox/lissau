<?php
/**
 * Description: Lionlab product variations
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

if ( have_rows('product_variations') ) : ?>

  <section id="product-variations" class="slider slider--variations padding--top">
    <div class="wrap hpad">
      <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
          <h2>Inspiration</h2>
        </div>
      </div>
      <div class="slider__track--variations is-slider">

        <?php
        // Loop through slides
        while ( have_rows('product_variations') ) :
          the_row();
          $image   = get_sub_field('img'); ?>

            <div class="slide__item--variations" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
              <a class="product__slider-link" href="<?= $image['sizes']['large']; ?>" data-fancybox="gallery-<?= $index; ?>" data-caption="<?= $image['caption']; ?>" itemprop="contentUrl" title="<?= $image['title'] ?>" data-width="<?= $image['sizes']['large-width']; ?>" data-height="<?= $image['sizes']['large-height']; ?>">   

                <img data-src="<?php echo esc_url($image['sizes']['variations']); ?>" src="<?php echo esc_url($image['sizes']['variations']); ?>" itemprop="thumbnail" alt="<?php echo esc_attr($image['alt']); ?>"> 

              </a>
            </div>

        <?php endwhile; ?>

      </div>
    </div>
  </section>
<?php endif; ?>