<?php  ?>

<section class="product-nav padding--top">
	<div class="wrap hpad">
		<div class="row">
			<nav class="product__nav col-sm-8 col-sm-offset-2 flex flex--valign flex--wrap">
				<?php if (have_rows('product_data') ) : ?>
				<a class="btn btn--brown product__nav-anchor" href="#product-data">Downloads</a>
				<?php endif; ?>

				<?php if (have_rows('product_variations') ) : ?>
				<a class="btn btn--brown product__nav-anchor" href="#product-variations">Inspiration</a>
				<?php endif; ?>

				<a class="btn btn--brown product__nav-anchor" href="#product-inquiry">Kontakt</a>
			</nav>
		</div>
	</div>
</section>