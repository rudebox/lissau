<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_404() ) {
		$title = __('Siden kunne ikke findes', 'lionlab');
	} elseif ( is_search() ) {
		$title = __('Søgeresultat', 'lionlab');
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}


	$text = get_field('page_text');
?>

<?php   
//get thumb
$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' ); 

if (is_page_template('page-layouts.php') &&  $thumb ) :
?>

<section class="slider slider--single">
	
  <div class="slider__item flex flex--valign" style="background-image: url(<?php echo $thumb[0]; ?>);">

  </div>

</section>
<?php endif; ?>

<?php if (is_single() ) : ?>
<section class="page__hero page__hero--single">
	<div class="wrap hpad center">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
<?php else : ?>
<section class="page__hero">
	<div class="wrap hpad center">
<?php endif; ?>
		<h1 class="page__title"><?php echo esc_html($title); ?></h1>
		<?php if (!is_post_type_archive() && !is_archive() ) :  ?>
		<?php echo $text; ?>
		<?php endif; ?>

		<?php if (is_post_type_archive('solafskaermning') ) :  ?>
			<?php echo category_description(3); ?>
		<?php endif; ?>

		<?php if (is_post_type_archive('av') ) :  ?>
			<?php echo category_description(4); ?>
		<?php endif; ?>

		<?php if (is_archive() ) : ?>
			<?php echo category_description(); ?>
		<?php endif; ?>
	</div>
</section>