<section class="archive__filter">
	<div class="wrap hpad flex flex--center flex--valign flex--wrap">
	  <h3 class="archive__filter-title">Vis mig alt i kategorien:</h3>
	
	  <div class="archive__filter-wrapper">
		<form id="js-archive-form" class="archive__form" action="<?php echo esc_url( home_url( '/' ) ); ?><?php echo $slug; ?>" method="get">

			<?php

				$args = array(
					'show_option_all' => __( 'AV' ),
					'show_count'       => 0,
					'child_of' 		   => 4,
					'orderby'          => 'name',
					'echo'             => 0,
					'class'			   => 'archive__filter-select archive__filter-select--av',
					'id'			   => 'js-archive-dropdown',
				);


				$slug = array (
					'value_field'	   => 'slug',
				);
			?>

			<?php $select  = wp_dropdown_categories( $args ); ?>
			<?php $replace = "<select$1 onchange='return this.form.submit()'>"; ?>
			<?php $select  = preg_replace( '#<select([^>]*)>#', $replace, $select ); ?>

			<?php echo $select; ?>

			<noscript>
				<input type="submit" value="View" />
			</noscript>

		</form>
		<i class="archive__filter-icon"><?php echo file_get_contents('wp-content/themes/lissau/assets/img/arrow-down.svg'); ?></i>

	  </div>
	</div>
</section>
