<?php
/**
 * Description: Lionlab intro field group
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');

//materials
$title = get_sub_field('intro_title');
$text = get_sub_field('intro_text');


//contact options
$phone = get_field('phone', 'options');
$mail = get_field('mail', 'options');
 
?>

<section class="intro bg--<?php echo esc_attr($bg); ?> padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<div class="intro__row row">

			<div class="intro__item intro__item--text col-sm-6 col-md-5 col-lg-4">
				<div class="intro__wrap bg--beige">
					<h2 class="intro__title"><?php echo $title; ?></h2>
					<?php echo $text; ?>
				</div>
				
				<div class="intro__contact flex flex--valign flex--wrap">
					<a class="btn btn--brown intro__btn" href="tel:<?php echo get_formatted_phone($phone); ?>"><?php echo esc_html($phone); ?></a>

					<a class="btn btn--brown intro__btn" href="mailto:<?php echo $mail; ?>"><?php echo esc_html($mail); ?></a>
				</div>
			</div>
			
	

			<div class="col-sm-6 col-md-5 col-lg-6 col-md-offset-2 col-lg-offset-2 intro__item intro__item--img">
				<?php 
					if (have_rows('intro_img') ) : while (have_rows('intro_img') ) : the_row(); 
					$img = get_sub_field('img');
				?>
					<img loading="lazy" class="intro__img <?php echo esc_attr($img_class); ?>" src="<?php echo esc_url($img['sizes']['cats']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
				<?php endwhile; endif; ?>
			</div>

		</div>
	</div>
</section>