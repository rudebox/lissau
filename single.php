<?php get_template_part('parts/header'); the_post(); ?>

<main>

  <?php   
    //get thumb
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' ); 
  ?>

  <section class="slider slider--single">
    <div class="slider__item flex flex--valign" style="background-image: url(<?php echo $thumb[0]; ?>);">

    </div>
    <?php get_template_part('parts/page', 'header'); ?>
  </section>

  <section class="single padding--both">
    <div class="wrap hpad">
      <div class="row">

        <article class="single__content col-sm-8 col-sm-offset-2" itemscope itemtype="http://schema.org/BlogPosting">

          <div itemprop="articleBody">
            <?php the_content(); ?>
          </div>  
          
          <div class="single__wrap center">
            <a class="btn btn--brown single__btn" onclick="window.history.go(-1); return false;">Tilbage</a>
          </div>

        </article>

      </div>

    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>