<?php 
/**
* Description: Lionlab product data repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

$blueprint = get_field('blueprint');

if (have_rows('product_data') ) :
?>

<section id="product-data" class="product padding--both">
	<div class="wrap hpad">
		<div class="row flex flex--wrap">
			
			<?php if ($blueprint) : ?>
			<div class="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-2 product__data-list">
			<?php else : ?>
			<div class="col-sm-8 col-sm-offset-2 product__data-list">
			<?php endif; ?>

				<h2 class="product__data-header">Downloads</h2>
				<?php while (have_rows('product_data') ) : the_row(); 
					$file = get_sub_field('pdf_file');
					$title = get_sub_field('data_title');
				?>

				<a target="_blank" href="<?php echo esc_url($file['url']); ?>" download class="product__data-item" itemscope itemtype="DigitalDocument">
					
					<p class="product__data-title"><?php echo esc_html($title); ?></p>
					<i class="product__data-icon"><?php echo file_get_contents('wp-content/themes/lissau/assets/img/download.svg'); ?></i>
				</a>
				<?php endwhile; ?>
			</div>
			
			<?php if ($blueprint) : ?>
			<div class="col-sm-8 col-sm-offset-2 col-md-3 col-md-offset-1 product__data-blueprint">
				<img class="product__data-img b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo esc_url($blueprint['url']); ?>" alt="<?php echo esc_attr($blueprint['alt']); ?>">
			</div>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php endif; ?>