<?php 
/**
* Description: Lionlab materials repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');

//materials
$title = get_sub_field('title');
$text = get_sub_field('text');
$img = get_sub_field('img');
$img_color = get_sub_field('img_bg_color');
$position = get_sub_field('position');

if ($position === 'left') {
	$img_class = 'col-sm-offset-3';
	$col_class = 'col-sm-offset-1 col-lg-offset-3';
}

if ($position === 'right') {
	$img_class = 'col-sm-pull-3';
}
?>

<section class="materials <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<div class="materials__row row flex flex--wrap <?php echo esc_attr($position); ?>">

			<div class="col-sm-5 col-lg-4 materials__item materials__item--img <?php echo esc_attr($position); ?> materials__item--img--<?php echo esc_attr($img_color); ?> is-animated">
				<?php if ($img) : ?>
				<img loading="lazy" class="materials__img <?php echo esc_attr($img_class); ?>" src="<?php echo esc_url($img['sizes']['side-img']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
				<?php endif; ?>
			</div>

			<div class="materials__item materials__item--text col-sm-6 col-lg-5 <?php echo esc_attr($col_class); ?> <?php echo esc_attr($position); ?>">
				<h3 class="materials__title"><?php echo $title; ?></h3>
				<?php echo $text; ?>
			</div>
			
		</div>
	</div>
</section>