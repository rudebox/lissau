<?php 
	$title = get_field('product_title');
	$desc = get_field('product_description');
 ?>

 <section id="product-desc" class="product padding--both">
 	<div class="wrap hpad">
 		<div class="row">
 			<div class="product__description col-sm-8 col-sm-offset-2">
 				<?php if ($title) : ?>
 				<h2 class="product__title"><?php echo esc_html($title); ?></h2>
 				<?php endif; ?>
 				<?php echo $desc; ?>
 			</div>
 		</div>
 	</div>
 </section>