<?php get_template_part('parts/header'); ?>

<main>

<?php get_template_part('parts/product', 'slider'); ?>

<?php get_template_part('parts/product', 'navigation'); ?>

<?php get_template_part('parts/product', 'description'); ?>

<?php get_template_part('parts/product', 'data'); ?>

<?php get_template_part('parts/product', 'variations'); ?>

<?php get_template_part('parts/product', 'inquiry'); ?>

</main>

<?php get_template_part('parts/footer'); ?>