<?php get_template_part('parts/header'); ?>

<main>

	<?php get_template_part('parts/page', 'header');?>
	
	<section class="padding--bottom">
		<div class="wrap hpad">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2 center">

					<p><?php echo __('Vi beklager, men siden du søgte efter findes ikke eller er flyttet.', 'lionlab') ?></p><br>

					<a class="btn btn--brown" href="/"><?php echo __('Gå til forsiden', 'lionlab') ?></a>

				</div>
			</div>
		</div>
	</section>

</main>

<?php get_template_part('parts/footer'); ?>