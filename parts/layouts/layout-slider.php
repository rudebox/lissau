<?php
/**
 * Description: Lionlab sliders
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

$title = get_sub_field('slides_title');
$caption = get_sub_field('slides_text');

if ( have_rows('slides') ) : ?>

  <section class="slider">
    <div class="slider__track is-slider">

      <?php
      // Loop through slides
      while ( have_rows('slides') ) :
        the_row();
        $image   = get_sub_field('slides_bg'); ?>

        <div loading="lazy" class="slider__item flex flex--valign" style="background-image: url(<?php echo esc_url($image['url']); ?>);">

        </div>

      <?php endwhile; ?>

    </div>
    <div class="slider__wrap">
      <div class="wrap hpad slider__container">
        <div class="slider__text center">
          <?php if ($title) : ?>
          <h2 class="slider__title h1"><?php echo $title; ?></h2>
          <?php endif; ?>
          <?php echo $caption; ?>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>